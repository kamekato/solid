package com.company;

public class Main {

    public static void main(String[] args) {
        Company company = new Company();
        company.addEmployee(new JavaDeveloper("Kazbek"));
        company.addEmployee(new JavaDeveloper("Murat"));
        company.addEmployee(new JavascriptDeveloper("Kaskelen"));
        company.addEmployee(new FullStackDeveloper("Aksay"));

        company.startWork();
    }
}
