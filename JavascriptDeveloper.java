package com.company;

public class JavascriptDeveloper extends Worker implements FrontendDeveloper, PrinositChai {

    public JavascriptDeveloper(String name) {
        super(name);
    }

    @Override
    public void develop() {
        writeFront();
    }

    @Override
    public void work() {
        develop();
        makeTea();
    }

    @Override
    public void writeFront() {
        System.out.println("My name is " + getName() +", I am writing Javascript code for Front!");
    }

    @Override
    public void makeTea() {
        System.out.println("Here is your cup of tea!");
    }
}
