package com.company;

public class JavaDeveloper extends Worker implements BackendDeveloper {

    public JavaDeveloper(String name) {
        super(name);
    }

    @Override
    public void develop() {
        writeBack();
    }

    @Override
    public void work() {
        develop();
    }

    @Override
    public void writeBack() {
        System.out.println("My name is " + getName() +", I am writing Java Code for backend");
    }
}
