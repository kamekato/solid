package com.company;

public abstract class Worker implements Employee {
    private String name;

    public Worker(String name) {
        setName(name);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}
