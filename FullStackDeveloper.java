package com.company;

public class FullStackDeveloper extends Worker implements FrontendDeveloper, BackendDeveloper {

    public FullStackDeveloper(String name) {
        super(name);
    }

    @Override
    public void writeBack() {
        System.out.println("I am writing back code in C++");
    }

    @Override
    public void develop() {
        System.out.println("My name is " + getName());
        writeFront();
        writeBack();
    }

    @Override
    public void work() {
        develop();
    }

    @Override
    public void writeFront() {
        System.out.println("I am writing front in CSS");
    }
}
